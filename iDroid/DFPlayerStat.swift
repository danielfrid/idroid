//
//  DFPlayerStat.swift
//  iDroid
//
//  Created by IT-Högskolan on 03/04/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

import Foundation

class DFPlayerStat{
    var playerScore : Float
    var playerName : String
    var playerLocation : String
    let lives: Int = 3
    var currentLives: Int?
  
    init(playerScore: Float, playerName: String, playerLocation: String) {
        self.playerScore = playerScore
        self.playerName = playerName
        self.playerLocation = playerLocation
        self.currentLives = self.lives
    }
}
