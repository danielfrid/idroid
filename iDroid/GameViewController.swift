//
//  GameViewController.swift
//  iDroid
//
//  Created by Daniel Frid on 31/03/15.
//  Copyright (c) 2015 Daniel Frid. All rights reserved.
//

import UIKit
import SpriteKit


class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = GameScene(size: view.bounds.size)
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        skView.presentScene(scene)
        skView.showsPhysics = false
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
}
