//
//  DFPhysicsTools.swift
//  iDroid
//
//  Created by Daniel Frid on 21/04/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

import Foundation
import SpriteKit

class DFPhysicsTools {

    static let None         :UInt32 = 0
    static let All          :UInt32 = UInt32.max
    static let TieFighter   :UInt32 = 0b1
    static let StarDestroyer:UInt32 = 0b10
    static let Player       :UInt32 = 0b100
    static let Laser        :UInt32 = 0b1000
    
    
    class func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
        
    }
    
    class func random(_ minVal:CGFloat, maxVal: CGFloat) -> CGFloat {
        let i = (maxVal-minVal)+minVal
        let result = random()*i
        return result
    }
    
    class func angleOfDirection(_ node: SKSpriteNode, distance: CGFloat) -> CGPoint{
        var projX = node.position.x
        var projY = node.position.y
        let radian = node.zRotation
        projX += cos(radian)*distance
        projY += sin(radian)*distance
        return CGPoint(x: projX, y: projY)
        
    }
    
    class func calcDirection(_ sprite: SKSpriteNode, distance: CGFloat) ->CGPoint {
        var projX = sprite.position.x
        var projY = sprite.position.y
        let radian = sprite.zRotation
        projX += cos(radian)*distance
        projY += sin(radian)*distance
        return CGPoint(x: projX, y: projY)
        
    }

}
