//
//  GameScene.swift
//  iDroid
//
//  Created by IT-Högskolan on 31/03/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    //set scene for paralax background
    let lowerXbounds: CGFloat = 0.0
    let lowerYbounds: CGFloat = 0.0
    var higherXbounds: CGFloat = 0.0
    var higherYbounds: CGFloat = 0.0
    var star_layer: [[SKSpriteNode]] = [] //obs! tvådimensionell array
    var star_layer_speed: [CGFloat] = []
    var star_layer_count: [Int] = []
    var star_layer_size: [CGSize] = []
    
    var starDirX: CGFloat = 0.5
    var starDirY: CGFloat = 0.2
    var deltaTime: CGFloat = 0.0166
    var time: CGFloat?
    
    var insertFromRight = true
    //initiate nodes
    let player = DFFlyingObject(imageNamed: "X_Wing", hp: 100)
    let rotateButtonR = SKSpriteNode(imageNamed: "rotate_right")
    let rotateButtonL = SKSpriteNode(imageNamed: "rotate_left")
    let fireButton = SKSpriteNode(imageNamed: "fire_button")
    let thrustButton = SKSpriteNode(imageNamed: "thrust_button")
    var scoreLabel = SKLabelNode(fontNamed: "Arial")
    let startButton = SKLabelNode(fontNamed: "Arial")
    let playAgainButton = SKLabelNode(fontNamed: "Arial")
    let header = SKLabelNode(fontNamed: "Arial")
    var tieFighters: [DFFlyingObject] = []
    var starDestroyers: [DFFlyingObject] = []
    var playerIsMoving = false
    var playerLives: [SKSpriteNode] = []
    var playerStat = DFPlayerStat(playerScore: 0, playerName: "Daniel", playerLocation: "Göteborg")
    
    override func didMove(to view: SKView) {
        self.backgroundColor = (SKColor.black)
        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        physicsWorld.contactDelegate = self

        header.text = "iDROID"
        header.fontSize = 30
        header.position = CGPoint(x: size.width/2, y: size.height/2)
        addChild(header)
        startButton.text = "START GAME"
        startButton.fontSize = 30
        startButton.fontColor = SKColor.red
        startButton.position = CGPoint(x: header.position.x, y: header.position.y-35)
        startButton.zPosition = 501
        addChild(startButton)
         let flashAction = SKAction.repeatForever( SKAction.sequence([SKAction.fadeIn(withDuration: 0.5),
            SKAction.wait(forDuration: 0.2),
            SKAction.fadeOut(withDuration: 0.5)]))
        startButton.run(flashAction)
      
        createNodes()
        resetLives()
    }
    
    // adds environment
    func createNodes(){
        createParalax()
        createControllerNodes()
        createPlayerNode()
    }
    
    
    func resetLives(){
        player.godModeCheck = 0
        playerStat.currentLives = playerStat.lives
        for _ in 1...playerStat.currentLives! {
            let life = SKSpriteNode(imageNamed: "X_Wing")
            life.size.height = 30
            life.size.width = 30
            life.zRotation = 0.78
            playerLives.append(life)
        }
        renderLives()

    }
    //start game
    func startBattle(){
        
        run(SKAction.repeatForever(
            SKAction.sequence([
                SKAction.run(addStarDestroyer),
                SKAction.wait(forDuration: 10.0)
                ])
            ), withKey: "enemies" )

    }
    
    func gameOver(){
        print("gameOver")
        for i in starDestroyers{
            i.removeFromParent()
        }
        starDestroyers = []
        for i in tieFighters{
            i.removeFromParent()
        }
        tieFighters = []
        removeAction(forKey: "enemies")

        header.text = "GAME OVER"
        header.fontSize = 30
        header.position = CGPoint(x: size.width/2, y: size.height/2)
        addChild(header)
        
        playAgainButton.text = "PLAY AGAIN"
        playAgainButton.fontSize = 30
        playAgainButton.fontColor = SKColor.red
        playAgainButton.position = CGPoint(x: header.position.x, y: header.position.y-35)
        addChild(playAgainButton)
        let flashAction = SKAction.repeatForever( SKAction.sequence([SKAction.fadeIn(withDuration: 0.5),
            SKAction.wait(forDuration: 0.2),
            SKAction.fadeOut(withDuration: 0.5)]))
        playAgainButton.run(flashAction)
        
    }


    func createParalax(){
        higherXbounds = self.frame.width
        higherYbounds = self.frame.height
        
        let star = SKSpriteNode(imageNamed: "star")
        star_layer = [[star], [star], [star]]
        star_layer_count.append(10)
        star_layer_speed.append(15.0)
        star_layer_size.append(CGSize(width: 3.0, height: 3.0))
        
        star_layer_count.append(20)
        star_layer_speed.append(10.0)
        star_layer_size.append(CGSize(width: 2.0, height: 2.0))
        
        star_layer_count.append(30)
        star_layer_speed.append(5.0)
        star_layer_size.append(star.size)
        
        //en for loop som körs tre gånger.
        for layer in 0...2 {
            //loop körs så många gånger som vi satt star_layour_count till
            for _ in 1...star_layer_count[layer]{
                let sprite = SKSpriteNode(imageNamed: "star")
                sprite.size = star_layer_size[layer]
                let x_pos = CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * higherXbounds
                let y_pos = CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * higherYbounds
                sprite.position = CGPoint(x: x_pos, y: y_pos)
                star_layer[layer].append(sprite)
                addChild(sprite)
            }
        }
    }
    func createPlayerNode(){
        player.size.height = 33;
        player.size.width = 33;
        player.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5)
        player.physicsBody = SKPhysicsBody(circleOfRadius: player.size.width/2)
        player.physicsBody?.isDynamic = true
        player.physicsBody?.linearDamping = 1.0
        player.physicsBody?.categoryBitMask = DFPhysicsTools.Player
        player.physicsBody?.contactTestBitMask = DFPhysicsTools.StarDestroyer | DFPhysicsTools.TieFighter
        player.physicsBody?.collisionBitMask = DFPhysicsTools.None
        addChild(player)
        player.godModeCheck = 0
        player.godMode = true
        player.godModeSwitch(player)
//        player.playerDevine(player)
        renderLives()
    }
    
    func renderLives(){
        var moveOnePos: CGFloat = 0
        if playerLives.count > 0 {
            for life in 0...playerLives.count-1 {
                playerLives[life].position = CGPoint(x: size.width/2+moveOnePos, y: 30)
                addChild(playerLives[life])
                moveOnePos += 30
            }
        }
    }
    
    func createControllerNodes() {

        rotateButtonL.name = "rotLeft"
        rotateButtonL.position = CGPoint(x: 40, y: 40)
        rotateButtonL.size.height = 80
        rotateButtonL.size.width = 80
        rotateButtonL.zPosition = 500
        addChild(rotateButtonL)
        
        rotateButtonR.name = "rotRight"
        rotateButtonR.position = CGPoint(x: 120, y: 40)
        rotateButtonR.size.height = 80
        rotateButtonR.size.width = 80
        rotateButtonR.zPosition = 500
        addChild(rotateButtonR)
        
        fireButton.name = "fire"
        fireButton.position = CGPoint(x: size.width-40, y: 40)
        fireButton.size.height = 80
        fireButton.size.width = 80
        fireButton.zPosition = 500
        addChild(fireButton)
        
        thrustButton.name = "thrust"
        thrustButton.position = CGPoint(x: size.width-40, y: 120)
        thrustButton.size.height = 80
        thrustButton.size.width = 80
        thrustButton.zPosition = 500
        addChild(thrustButton)
        
        scoreLabel.text = ("SCORE:")
        scoreLabel.fontSize = 20.0
        scoreLabel.position.x = size.width/2
        scoreLabel.position.y = size.height-30
        scoreLabel.zPosition = 500
        addChild(scoreLabel)
        

    }
    
    
    // StarDestroyer insertion
    func addStarDestroyer() {
        
        //random plats på Y-axeln
        let starDestroyer = DFFlyingObject(imageNamed: "StarDestroyerL", hp: 100.00)
        let path:CGMutablePath = CGMutablePath()
        let offsetX: CGFloat = 110.0
        let offsetY: CGFloat = 60

		path.move(to: CGPoint(x: -3 - offsetX, y: 64 - offsetY))
		path.addLine(to: CGPoint(x: 99 - offsetX, y: 129 - offsetY))
		path.addLine(to: CGPoint(x: 212 - offsetX, y: 7 - offsetY))
        
        path.closeSubpath();
        
        starDestroyer.physicsBody = SKPhysicsBody(polygonFrom: path)
       // starDestroyer.physicsBody = SKPhysicsBody(circleOfRadius: self.size.width/4)
        starDestroyer.physicsBody?.isDynamic = true
        starDestroyer.physicsBody?.categoryBitMask = DFPhysicsTools.StarDestroyer
        starDestroyer.physicsBody?.contactTestBitMask = DFPhysicsTools.Laser | DFPhysicsTools.Player
        starDestroyer.physicsBody?.collisionBitMask = DFPhysicsTools.None
        
        
        let actualY = DFPhysicsTools.random(starDestroyer.size.height/2, maxVal: size.height - starDestroyer.size.height/2)
        
        //true = starships-position högerisida. Annars vänster.
        insertFromRight = !insertFromRight
        starDestroyer.insertFromRight = insertFromRight
        if(insertFromRight){
            starDestroyer.xScale = -1.0  //flip Node if coming from Right
            starDestroyer.position = CGPoint(x: size.width + starDestroyer.size.width/2, y: actualY)
        } else {
            starDestroyer.position = CGPoint(x: 0 - starDestroyer.size.width/2, y: actualY)
        }
        addChild(starDestroyer)
        starDestroyers.append(starDestroyer)
        starDestroyer.setEmitterEffectDestroyer()
        
        // Determine speed of the starship
        let actualDuration = DFPhysicsTools.random(CGFloat(30.0), maxVal: CGFloat(50.0))
        // Create destination point
        var moveToPoint = CGPoint(x: -starDestroyer.size.width/2, y: actualY)
        if(!insertFromRight){
           moveToPoint = CGPoint(x: size.width + starDestroyer.size.width, y: actualY)
        }
        //set action for starship
        let actionMove = SKAction.move(to: moveToPoint, duration: TimeInterval(actualDuration))
        //      let actionMoveDone = SKAction.removeFromParent()
        let moveToOrigin = SKAction.move(to: starDestroyer.position, duration: 0)
        let sequence = SKAction.sequence([actionMove, moveToOrigin])
        let actionToRun = SKAction.repeatForever(sequence)
        starDestroyer.run(actionToRun)
    
    }
    
    // Tie Fighter insertion
    func unleashTieFighter(_ birthPoint: CGPoint){
        let tieFighter = DFFlyingObject(imageNamed: "TIE_Fighter", hp: 25.00)
        tieFighter.tieThrust = DFPhysicsTools.random(20.0, maxVal: 50.0)
        tieFighter.size.width = 35.0
        tieFighter.size.height = 30.0
        tieFighter.position = birthPoint
        addChild(tieFighter)
        tieFighters.append(tieFighter)
        tieFighter.physicsBody = SKPhysicsBody(circleOfRadius: tieFighter.size.width/2)
        tieFighter.physicsBody?.usesPreciseCollisionDetection = true
        tieFighter.physicsBody?.isDynamic = true
        tieFighter.physicsBody?.categoryBitMask = DFPhysicsTools.TieFighter
        tieFighter.physicsBody?.contactTestBitMask = DFPhysicsTools.Player | DFPhysicsTools.Laser
        tieFighter.physicsBody?.collisionBitMask = DFPhysicsTools.None
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touched me!")
        let touch = touches.first
        let touchLocation = touch!.location(in: self)
        let touchedNode = self.atPoint(touchLocation)

        if (startButton.contains(touchLocation)) {
            startBattle()
            startButton.removeFromParent()
            header.removeFromParent()
            player.godMode = true
            player.godModeSwitch(player)
        }
        if (playAgainButton.contains(touchLocation)){
            playAgainButton.removeFromParent()
            header.removeFromParent()
            //createNodes()
            createPlayerNode()
            resetLives()
            startBattle()
        }

        if(touchedNode.name == "fire"){
            //let projectile = DFFlyingObject(imageNamed: "laserBlue01", hp: 1.0)
            let projectile = DFFlyingObject(imageNamed: "projectile", hp: 1.0)
            let projectileE = SKEmitterNode(fileNamed: "fireShot")
            projectileE!.zRotation = 1.5707963268
            projectile.physicsBody = SKPhysicsBody(circleOfRadius: 10.00)
            projectile.physicsBody?.isDynamic = true
            projectile.physicsBody?.categoryBitMask = DFPhysicsTools.Laser
            projectile.physicsBody?.contactTestBitMask = DFPhysicsTools.StarDestroyer | DFPhysicsTools.TieFighter
            projectile.physicsBody?.collisionBitMask = DFPhysicsTools.None
            projectile.physicsBody?.usesPreciseCollisionDetection = true
            projectile.position = player.position
            projectile.zRotation = player.zRotation
            projectile.addChild(projectileE!)
            addChild(projectile)
        
            //Get a position for laser beam to move to.
            let endPos = DFPhysicsTools.angleOfDirection(projectile, distance: 1000)
            
            //arrange action
            let actionMove = SKAction.move(to: endPos, duration: 3)
            let actionSound = SKAction.playSoundFileNamed("XWing-Laser.wav", waitForCompletion: false)
            let actionMoveDone = SKAction.removeFromParent()
            projectile.run(SKAction.sequence([actionSound, actionMove, actionMoveDone]))
        }
        if(touchedNode.name == "rotLeft"){
            let actionRotLeft = SKAction.rotate(byAngle: CGFloat(3.14), duration: 1)
            player.run(SKAction.repeatForever(actionRotLeft))
        }
        if(touchedNode.name == "rotRight"){
            let actionRotRight = SKAction.rotate(byAngle: CGFloat(-M_PI), duration: 1)
            player.run(SKAction.repeatForever(actionRotRight))
        }
        if(touchedNode.name == "thrust"){
            playerIsMoving = true
            let thrustNode = SKEmitterNode(fileNamed: "xwing_thrust")
            thrustNode!.position = CGPoint(x: -20, y: 0)
            player.addChild(thrustNode!)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let touchLocation = touch!.location(in: self)
        let touchedNode = self.atPoint(touchLocation)
        if(touchedNode.name == "rotLeft" || touchedNode.name == "rotRight"){
                player.removeAllActions()
        }
        if touchedNode.name == "thrust" {
            playerIsMoving = false
            player.removeAllChildren()
        }
    }
    
    func movePlayer(){
        let nextPos = DFPhysicsTools.angleOfDirection(player, distance: 50.0)
        let x: CGFloat = player.position.x - nextPos.x
        let y: CGFloat = nextPos.y - player.position.y
        let direction: CGFloat = atan2(x, y) + CGFloat(M_PI_2)
        let velX: CGFloat = player.xwingThrust * cos(direction)
        let velY: CGFloat = player.xwingThrust * sin(direction)
        let vector = CGVector(dx: velX, dy: velY)
        player.physicsBody?.velocity = vector
    }

    //
    //  Stardestroyer get hit!
    //
    func destroyerHit(_ laser: DFFlyingObject, enemy: DFFlyingObject, contactPoint: CGPoint){
        print("Hit!!")
        enemy.hp -= 25
        laser.removeFromParent()
        //if starship is dead
        if enemy.hp <= 0 {
        let burstNode = SKEmitterNode(fileNamed: "MyXpark")
        burstNode!.position = enemy.position // contactPoint
        enemy.removeFromParent()
        self.addChild(burstNode!)
        run(SKAction.playSoundFileNamed("explosion-01.wav", waitForCompletion: false))
        playerStat.playerScore += 10
        unleashTieFighter(enemy.position)
        unleashTieFighter(enemy.position)
        } else {
            let fireNode = SKEmitterNode(fileNamed: "hitFire")
            fireNode!.position = contactPoint
            fireNode!.zPosition = 500
            self.addChild(fireNode!)
        }
        scoreLabel.text = "SCORE: \(playerStat.playerScore)"

    }
    
    //
    //  Tiefighter get hit!
    //
    func tieHit(_ laser: DFFlyingObject, enemy: DFFlyingObject, contactPoint: CGPoint){
        print("Hit!!")
        
        let burstNode = SKEmitterNode(fileNamed: "tieExplosion")
        burstNode!.position = contactPoint
        enemy.removeFromParent()
        laser.removeFromParent()
        self.addChild(burstNode!)
        run(SKAction.playSoundFileNamed("explosion-01.wav", waitForCompletion: false))
        
        playerStat.playerScore += 1
        scoreLabel.text = "SCORE: \(playerStat.playerScore)"
        
    }
    
    //
    //  Player get hit!
    //
    func playerGotHit(_ player: DFFlyingObject, enemy: DFFlyingObject, killEnemy: Bool, contactPoint: CGPoint){
        print("AJ!!")
        let burstNode = SKEmitterNode(fileNamed: "MyXpark")
        burstNode!.position = contactPoint
        if killEnemy { enemy.removeFromParent() }
        player.removeFromParent()
        self.addChild(burstNode!)
        run(SKAction.playSoundFileNamed("explosion-01.wav", waitForCompletion: false))
        print("Före \(playerLives.count)")
        for life in 0...playerLives.count-1 {
            playerLives[life].removeFromParent()
        }
        playerLives.removeLast()
       
        playerStat.currentLives! -= 1
        if playerStat.currentLives == 0 { gameOver() }
        else { createPlayerNode() }
        
        
    }

    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        //Make sure player/laser are firstBody and enemy secondBody
        if contact.bodyA.categoryBitMask > contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        //if Laser hit Stardestroyer or TieFighter
        if (firstBody.categoryBitMask == DFPhysicsTools.Laser) &&
            (secondBody.categoryBitMask == DFPhysicsTools.StarDestroyer) {
                let contactPoint = contact.contactPoint
                if let bodyA = firstBody.node as? DFFlyingObject, let bodyB = secondBody.node as? DFFlyingObject {
                    destroyerHit(bodyA, enemy: bodyB, contactPoint: contactPoint)
                } else {
                    NSLog("One of the bodies was nil")
                }

            }
        
        if (firstBody.categoryBitMask == DFPhysicsTools.Laser) &&
            (secondBody.categoryBitMask == DFPhysicsTools.TieFighter) {
                let contactPoint = contact.contactPoint
                if let bodyA = firstBody.node as? DFFlyingObject, let bodyB = secondBody.node as? DFFlyingObject {
                    tieHit(bodyA, enemy: bodyB, contactPoint: contactPoint)
                } else {
                    NSLog("One of the bodies was nil")
                }
        }
        //if Stardestroyer or TieFighter runs over Player
        if(firstBody.categoryBitMask == DFPhysicsTools.Player) &&
            (secondBody.categoryBitMask == DFPhysicsTools.StarDestroyer){
                let contactPoint = contact.contactPoint
                playerGotHit(firstBody.node as! DFFlyingObject, enemy: secondBody.node as! DFFlyingObject,killEnemy: false, contactPoint: contactPoint)
        }
        
        if(firstBody.categoryBitMask == DFPhysicsTools.Player) &&
            (secondBody.categoryBitMask == DFPhysicsTools.TieFighter){
                let contactPoint = contact.contactPoint
                if let bodyA = firstBody.node as? DFFlyingObject, let _ = secondBody.node as? DFFlyingObject {
                playerGotHit(firstBody.node as! DFFlyingObject, enemy: secondBody.node as! DFFlyingObject, killEnemy: true, contactPoint: contactPoint)
                } else {
                    print("one of the body was nil")
                }
        }

    }
    
    override func update(_ currentTime: TimeInterval) {
     //   println(player.physicsBody!.categoryBitMask)
        
        //check if GodMode
        if player.godMode {
            if player.godModeCheck == 0{
                player.godModeCheck = currentTime}
            if ((player.godModeCheck + player.godModeTime) < currentTime) {
            player.godMode = false
            player.godModeSwitch(player)
            }
        }
        
        if player.position.x < 0  { player.position.x = 0}
        if player.position.x > size.width { player.position.x = size.width}
        if player.position.y < 0  { player.position.y = 0}
        if player.position.y > size.height { player.position.y = size.height}
        
        //move all tiefighters towards player
        for DFFlyingObject in tieFighters {
            let x: CGFloat = DFFlyingObject.position.x - player.position.x
            let y: CGFloat = player.position.y - DFFlyingObject.position.y
            let direction: CGFloat = atan2(x, y) + CGFloat(M_PI_2)
            let velX: CGFloat = DFFlyingObject.tieThrust! * cos(direction)
            let velY: CGFloat = DFFlyingObject.tieThrust! * sin(direction)
            let vector = CGVector(dx: velX, dy: velY)
            DFFlyingObject.physicsBody?.velocity = vector
        }
        
        if playerIsMoving {movePlayer()}
        //move backgroundlayers
        var dt: CGFloat = 0.0
        if let previousT = time {
            dt = CGFloat(currentTime) - previousT
        }
        time = CGFloat(currentTime)
        for index in 0...star_layer.count-1{
            moveStarLayer(star_layer[index], speed: star_layer_speed[index], dt: dt)
        }
    }
    
    
    func moveStarLayer(_ starLayer: [SKSpriteNode], speed: CGFloat, dt: CGFloat){
        var sprite: SKSpriteNode
        var newX: CGFloat = 0.0
        var newY: CGFloat = 0.0
        
        for index in 0...starLayer.count-1 {
            sprite = starLayer[index]
            newX = sprite.position.x + starDirX * speed * dt
            newY = sprite.position.y + starDirY * speed * dt
            
            sprite.position = checkBounds(CGPoint(x: newX, y: newY))
        }
    }
    
    
    func checkBounds(_ current:CGPoint) -> CGPoint{
        var x = current.x
        var y = current.y
        
        if x<0 { x += higherXbounds}
        if y<0 { y += higherYbounds}
        if x>higherXbounds { x -= higherXbounds}
        if y>higherYbounds { y -= higherYbounds}
        
        return CGPoint(x: x, y: y)
    }
    
    
    
}
