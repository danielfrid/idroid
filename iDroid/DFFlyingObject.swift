//
//  DFFlyingObject.swift
//  iDroid
//
//  Created by Daniel Frid on 08/04/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//



import Foundation
import SpriteKit

class DFFlyingObject: SKSpriteNode {
    var hp: CGFloat = 0.0
    var insertFromRight: Bool?
    var xwingThrust: CGFloat = 50.0
    var tieThrust: CGFloat?
    var godMode: Bool = false
    var godModeTime: TimeInterval = 5
    var godModeCheck: TimeInterval = 0
    required init?(coder aDecoder: NSCoder) {
        fatalError("initfrackin Error")
    }
  
    init(imageNamed: String, hp: CGFloat){
        let imageTexture = SKTexture(imageNamed: imageNamed)
        super.init(texture: imageTexture, color: UIColor.clear, size: imageTexture.size())

        self.hp = hp
   
    }
    
    func setEmitterEffectDestroyer(){
        let flameNode = SKEmitterNode(fileNamed: "MuFire")
        flameNode!.position = CGPoint(x: -40, y: 18.0)
        flameNode!.zRotation = -2.8
        flameNode!.zPosition = -1
        self.addChild(flameNode!)
    }
    
    // for testing purpose. Not used after implementing godModeSwitch .
    func playerDevine(_ devine: DFFlyingObject){
        devine.physicsBody?.categoryBitMask = 666
        let i:Int = 0
        
        run(SKAction.repeat(
            SKAction.sequence([
                SKAction.run({devine.isHidden = !devine.isHidden
                    
                    if i==16
                    { devine.physicsBody?.categoryBitMask = DFPhysicsTools.Player}
                }),
                SKAction.wait(forDuration: 0.2)
                ]), count: 16
            ))
    }
    
    func godModeSwitch(_ god: DFFlyingObject){
        if godMode {
            god.physicsBody?.categoryBitMask = 666
            let flashAction = SKAction.repeatForever( SKAction.sequence([SKAction.fadeOut(withDuration: 0.2),
                SKAction.wait(forDuration: 0.1),
                SKAction.fadeIn(withDuration: 0.2)]))
        
            god.run(flashAction, withKey: "godlyTwink")
        }
        else {
            god.physicsBody?.categoryBitMask = DFPhysicsTools.Player
            god.alpha = 1.0
            god.removeAction(forKey: "godlyTwink")
        }
    }

    
}
